<?php
define('ROOT_PATH', dirname( __FILE__ ) . '/');
define('MAIN_TEMPLATE', ROOT_PATH.'local/templates/tech_landing/');

define('SITE_DIR', '/');
define('SITE_TEMPLATE_PATH', SITE_DIR.'local/templates/tech_landing/');
define('IMGS_DIR', SITE_TEMPLATE_PATH.'images/');

function getPath() {
	$parts = explode('?', $_SERVER["REQUEST_URI"]);
	return $parts[0];
}