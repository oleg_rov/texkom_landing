const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const del = require('del');
const gulpif = require('gulp-if');
const concat = require('gulp-concat');
const gcmq = require('gulp-group-css-media-queries');

const mainTemplate = 'local/templates/tech_landing/';

const path = {
	src: {
		sass: 'source/styles',
	},
	build: {
        js: 'build/js',
        css: 'build/css'
	},
	watch: {
		styles: 'source/styles/**/*.scss',
		scripts: 'source/scripts/**/*.js',
	}
};

const jsFiles = [
	mainTemplate + 'source/scripts/libs/jquery-3.4.1.min.js',
	mainTemplate + 'source/scripts/libs/jquery.cookie.js',
	mainTemplate + 'source/scripts/libs/jquery.inputmask.js',
	mainTemplate + 'source/scripts/script.js'
];

const isDev = (process.argv.indexOf('--dev') !== -1);
const isProd = !isDev;

function styles() {
	return gulp.src(mainTemplate + path.src.sass + '/main.scss')
			.pipe(gulpif(isDev, sourcemaps.init()))
			.pipe(sass())
			.pipe(rename('main.min.css'))
			.pipe(autoprefixer({
		            browsers: ['> 0.1%'],
		            cascade: false
		        }))
			.pipe(gulpif(isProd, gcmq()))
			.pipe(cleanCSS({
			   		level: 2
			   }))
			.pipe(gulpif(isDev, sourcemaps.write()))
			.pipe(gulp.dest(mainTemplate + path.build.css))
			.pipe(browserSync.stream());
}

function scripts() {
	return gulp.src(jsFiles)
			.pipe(gulpif(isDev, sourcemaps.init()))
			.pipe(concat('script.min.js'))
			.pipe(uglify({
				toplevel: true
			}))
			.pipe(gulpif(isDev, sourcemaps.write()))
			.pipe(gulp.dest(mainTemplate + path.build.js))
			.pipe(browserSync.stream());
}

function watch() {
   browserSync.init({
	proxy: "texkom.land"
  });

  gulp.watch(mainTemplate + path.watch.styles, styles)
  gulp.watch(mainTemplate + path.watch.scripts, scripts)
  gulp.watch("./*.php").on('change', browserSync.reload);

  gulp.watch("./templates/tech/*.php").on('change', browserSync.reload);
}

function clean() {
   return del([mainTemplate + 'build/*']);
}

gulp.task('styles', styles);
gulp.task('scripts', scripts);

gulp.task('del', clean);
gulp.task('build', gulp.series(clean, gulp.parallel(styles,scripts)));

gulp.task('watch', gulp.series('build', watch));
